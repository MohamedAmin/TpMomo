package com.formation.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.formation.bean.Article;

public class ArticleDAO {

	public static void create(Article a) throws SQLException, Exception {
		String nom = a.getNom();
		String desc = a.getDescription();
		Float prix = a.getPrix(); 
		String req2 = "insert INTO `article`(`nom`, `description`, `prix`) VALUES ('"+nom+"','"+desc+"',"+prix+")";
		try (Connection c = ConnectionDAO.connexion()) {
			try (Statement statement = c.createStatement()) {
				statement.executeUpdate(req2);
				
				}

			}
		}
	
	
	public static void delete(Article a) throws SQLException, Exception {
		String nom = a.getNom();
		String desc = a.getDescription();
		Float prix = a.getPrix(); 
		String req2 = "DELETE FROM `article` WHERE id = "+a.getId();
		try (Connection c = ConnectionDAO.connexion()) {
			try (Statement statement = c.createStatement()) {
				statement.executeUpdate(req2);
				
				}

			}
		}
	
	public static void update(Article a) throws SQLException, Exception {
		String nom = a.getNom();
		String desc = a.getDescription();
		Float prix = a.getPrix(); 
		String req2 =	"UPDATE `article` SET `nom`='"+nom+"',`description`='"+desc+"',`prix`='"+prix+"' WHERE id = "+a.getId();
		try (Connection c = ConnectionDAO.connexion()) {
			try (Statement statement = c.createStatement()) {
				statement.executeUpdate(req2);
				
				}

			}
	}
	
	
	public static ArrayList<Article> findAll() throws SQLException, Exception {
		ArrayList<Article> articles = new ArrayList<>();
		String req2 = "Select * from article";
		try (Connection c = ConnectionDAO.connexion()) {

			try (Statement statement = c.createStatement()) {
				ResultSet resultSet = statement.executeQuery(req2);
				while (resultSet.next()) {
					Article arti = new Article();
					arti.setId(resultSet.getInt("id"));
					arti.setNom(resultSet.getString("nom"));
					arti.setPrix(resultSet.getFloat("prix"));
					arti.setDescription(resultSet.getString("description"));
					articles.add(arti);
				}
			}
		}
		return articles;
	}
	
	public static Article find(int id) throws SQLException, Exception {
		String req2 = "Select * from article where id = "+id;
		try (Connection c = ConnectionDAO.connexion()) {

			try (Statement statement = c.createStatement()) {
				ResultSet resultSet = statement.executeQuery(req2);
				resultSet.next();
					Article arti = new Article();
					arti.setId(resultSet.getInt("id"));
					arti.setNom(resultSet.getString("nom"));
					arti.setPrix(resultSet.getFloat("prix"));
					arti.setDescription(resultSet.getString("description"));
					return arti;

				}

			}
	}
	
	
}
