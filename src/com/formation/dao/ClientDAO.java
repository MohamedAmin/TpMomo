package com.formation.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.formation.bean.Article;
import com.formation.bean.User;

public class ClientDAO {

	public static void create(User a) throws SQLException, Exception {
		String nom = a.getNom();
		String prenom = a.getPrenom();
		String email = a.getEmail();
		String mdp = a.getMdp();
		String dateDeNaissance = a.getDateDeNaissance();

		String req2 = "insert INTO `user`(`nom`, `prenom`, `email`, `mdp`, `dateDeNaissance`) VALUES ('"+nom+"','"+prenom+"','"+email+"','"+mdp+"','"+dateDeNaissance+"')";
		try (Connection c = ConnectionDAO.connexion()) {
			try (Statement statement = c.createStatement()) {
				statement.executeUpdate(req2);
				
				}

			}
		}
	
	
	public static void delete(User a) throws SQLException, Exception {

		
		String req2 = "DELETE FROM `user` WHERE email = '"+a.getEmail()+"'";
		try (Connection c = ConnectionDAO.connexion()) {
			try (Statement statement = c.createStatement()) {
				statement.executeUpdate(req2);
				
				}

			}
		}
	
	public static void update(User a) throws SQLException, Exception {
		String nom = a.getNom();
		String prenom = a.getPrenom();
		String email = a.getEmail();
		String mdp = a.getMdp();
		String dateDeNaissance = a.getDateDeNaissance();

		String req2 =	"UPDATE `user` SET `nom`='"+nom+"',`prenom`='"+prenom+"',`dateDeNaissance`='"+dateDeNaissance+"' WHERE email = '"+a.getEmail()+"'";
		try (Connection c = ConnectionDAO.connexion()) {
			try (Statement statement = c.createStatement()) {
				statement.executeUpdate(req2);
				
				}

			}
	}
	
	
	public static ArrayList<User> findAll() throws SQLException, Exception {
		
		ArrayList<User> users = new ArrayList<>();
		String req2 = "Select * from user";
		try (Connection c = ConnectionDAO.connexion()) {

			try (Statement statement = c.createStatement()) {
				ResultSet resultSet = statement.executeQuery(req2);
				while (resultSet.next()) {
					User u = new User();
					u.setEmail(resultSet.getString("email"));
					u.setNom(resultSet.getString("nom"));
					u.setDateDeNaissance(resultSet.getString("dateDeNaissance"));
					u.setMdp(resultSet.getString("mdp"));
					u.setPrenom(resultSet.getString("prenom"));
					users.add(u);
				}

			}
		}
		return users;
	}
	

	public static User find(String email) throws SQLException, Exception {
		
		String req2 = "Select * from user email = '"+email+"'";
		try (Connection c = ConnectionDAO.connexion()) {

			try (Statement statement = c.createStatement()) {
				ResultSet resultSet = statement.executeQuery(req2);
				resultSet.next();
					User u = new User();
					u.setEmail(resultSet.getString("email"));
					u.setNom(resultSet.getString("nom"));
					u.setDateDeNaissance(resultSet.getString("dateDeNaissance"));
					u.setMdp(resultSet.getString("mdp"));
					u.setPrenom(resultSet.getString("prenom"));
					return u;
				

			}
		}
	}
}
