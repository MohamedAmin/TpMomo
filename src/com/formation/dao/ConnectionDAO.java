package com.formation.dao;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public class ConnectionDAO {
	public static Connection connexion() throws Exception {
		Properties props = new Properties();
		System.out.println(System.getProperty("user.dir"));
		try (FileInputStream fis = new FileInputStream("C:\\Users\\Formation\\eclipse-workspace\\TpMomo\\conf.properties")) {
			props.load(fis);
		}
		Class.forName(props.getProperty("jdbc.driver.class"));
		String url = props.getProperty("jdbc.url");
		String user = props.getProperty("jdbc.login");
		String password = props.getProperty("jdbc.password");
		Connection connection = DriverManager.getConnection(url, user, password);
		return connection;
	}
}
