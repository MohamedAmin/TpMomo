package com.formation.controlleur;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.formation.bean.User;

/**
 * Servlet implementation class InscriptionServelet
 */
@WebServlet("/inscription")
public class InscriptionServelet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InscriptionServelet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.getServletContext().getRequestDispatcher("/WEB-INF/inscription.jsp").forward(request, response);
}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		ArrayList<User> users = (ArrayList<User>) this.getServletContext().getAttribute("users");
		System.out.println("users size = "+users.size());
		User user = new User(request.getParameter("nom"),request.getParameter("prenom"),request.getParameter("email"),
				request.getParameter("mdp"),request.getParameter("birthday")
				);
		
		
		users.add(user);
		
		response.sendRedirect("home");
	}

}
