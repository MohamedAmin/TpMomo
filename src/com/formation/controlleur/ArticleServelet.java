package com.formation.controlleur;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.formation.bean.Article;
import com.formation.dao.ArticleDAO;

/**
 * Servlet implementation class ArticleServelet
 */
@WebServlet("/article")
public class ArticleServelet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ArticleServelet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String action = request.getParameter("action");
		System.out.println(action);
		String id = request.getParameter("id");
		if (action == null) {
			if(id == null) {
				request.removeAttribute("article");
			try {
				ArrayList<Article> as = ArticleDAO.findAll();
				System.out.println(as.size());
				request.setAttribute("articles", as);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("lalala");
			this.getServletContext().getRequestDispatcher("/WEB-INF/views/all_article.jsp").forward(request, response);
			return;}
			else {
				Article a;
				try {
					a = ArticleDAO.find(Integer.parseInt(id));
					request.setAttribute("article", a);

				}  catch (Exception e) {}
				this.getServletContext().getRequestDispatcher("/WEB-INF/views/all_article.jsp").forward(request, response);
			}
		}	
		else if (action.equals("delete")) {
			if (id != null) {
				Article a = new Article();
				a.setId(Integer.parseInt(id));
				try {
					ArticleDAO.delete(a);
				} catch (Exception e) {
				}
			}
			response.sendRedirect("article");

		} else if (action.equals("update")) {
			if (id != null) {
				Article a;
				try {
					a = ArticleDAO.find(Integer.parseInt(id));
					request.setAttribute("article", a);
				} catch (Exception e) {
				}

				this.getServletContext().getRequestDispatcher("/WEB-INF/views/update_article.jsp").forward(request,
						response);
			}
		} else if (action.equals("create")) {
			System.out.println("bou");

			this.getServletContext().getRequestDispatcher("/WEB-INF/views/create_article.jsp").forward(request,
					response);

		} 
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String action = request.getParameter("action");
		if (action.equals("create")) {
			String nom = request.getParameter("nom");
			String description = request.getParameter("description");
			Float prix = Float.parseFloat(request.getParameter("prix"));
			Article a = new Article();
			a.setDescription(description);
			a.setNom(nom);
			a.setPrix(prix);
			try {
				ArticleDAO.create(a);
			} catch (Exception e) {
			}
		}
		if (action.equals("update")) {
			String id = request.getParameter("id");
			String nom = request.getParameter("nom");
			String description = request.getParameter("description");
			Float prix = Float.parseFloat(request.getParameter("prix"));
			Article a = new Article();
			a.setId(Integer.parseInt(id));
			a.setDescription(description);
			a.setNom(nom);
			a.setPrix(prix);
			try {
				ArticleDAO.update(a);
			} catch (Exception e) {
			}
		}
		response.sendRedirect("article");
	}

}
