package com.formation.controlleur;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.formation.bean.User;

/**
 * Servlet implementation class LoginServelet
 */
@WebServlet("/login")
public class LoginServelet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginServelet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html; charset=utf8");

		request.setAttribute("title", "home");
		request.setAttribute("container", "index.jsp");
		// request.getSession().setAttribute("user", "aaa");
		// this.getServletContext().getRequestDispatcher("/WEB-INF/index.jsp").forward(request,
		// response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		ArrayList<User> users = (ArrayList<User>) this.getServletContext().getAttribute("users");
		String s = (String) request.getParameter("email");
		String s2 = (String) request.getParameter("mdp");

		User u = ExisteDansList(users,s,s2);
		if (u != null) {
			request.getSession().setAttribute("user", u);
			System.out.println("normalementOk");
			response.sendRedirect("home");

		} else {
			request.getSession().setAttribute("message", "Erreur de connexion");
			response.sendRedirect("connexion");
		}

		// this.getServletContext().getRequestDispatcher("/WEB-INF/connexion.jsp").forward(request,
		// response);

	}

	private static User ExisteDansList(ArrayList<User> l, String email, String mdp) {
		for (User a : l) {
			if (a.getEmail().equals(email)) {
				if (a.getMdp().equals(mdp)) {
					System.out.println("connecter");
					return a;
				}

			}
		}
		System.out.println("pas connecter");

		return null;
	}

}
