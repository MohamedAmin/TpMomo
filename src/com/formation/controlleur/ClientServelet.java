package com.formation.controlleur;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.formation.bean.Article;
import com.formation.bean.User;
import com.formation.dao.ArticleDAO;
import com.formation.dao.ClientDAO;

/**
 * Servlet implementation class ClientServelet
 */
@WebServlet("/client")
public class ClientServelet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ClientServelet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		String email = request.getParameter("email");

		if(action.equals("delete")) 
			if(email != null) {
				User a = new User();
				a.setEmail(email);
				try {
					ClientDAO.delete(a);
				} catch (Exception e) {}
			}
		if(action.equals("update")) 
			if(email != null) {
				User a;
				try {
					a = ClientDAO.find(email);
					request.setAttribute("resUser", a);
				} catch (Exception e) {}
				
				this.getServletContext().getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);
			}
			else
		if(action.equals("create")) 
				this.getServletContext().getRequestDispatcher("/WEB-INF/create_article.jsp").forward(request, response);
		else
			this.getServletContext().getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);

		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
