package com.formation.listener;

import java.util.ArrayList;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import com.formation.bean.Article;
import com.formation.bean.User;


@WebListener
public class Listener implements ServletContextListener {
	
	

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {

		System.out.println("DESTRUCTION DU CONTEXTE");
		
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		ArrayList<User> users = new ArrayList<>();
		User ut = new User("niwa","momo","momo@gmail.com","mdp","");
		users.add(ut);
		arg0.getServletContext().setAttribute("users", users);
		arg0.getServletContext().setAttribute("ut", ut);
		
		
		ArrayList<Article> articles = new ArrayList<>();
		for(int i = 0 ; i < 10;i++) {
			Article a = new Article(0,"Tv_"+i,"decription_"+i, (float) (10 + i));
			articles.add(a);
		}
		
		arg0.getServletContext().setAttribute("articles", articles);

		
		System.out.println("CREATION DU CONTEXTE");
		System.out.println(ut.getNom());
		
	}
	 
	 
	
}

