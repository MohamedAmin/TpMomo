<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
        <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<h1>index</h1>
	<a href="article">VERS LA DEUXIEME PARTI DU TP AVEC LES BASES DE DONNEE</a>
	<br>
	<br>
	    <c:if test="${ not empty sessionScope.user }" >
	    <p>Bonjour 	${sessionScope.user}</p>
	    	  	<a href="logout">Se deconnecter</a>
	    
	</c:if>
	  <c:if test="${ empty sessionScope.user }" >
	  	<a href="connexion">Se connecter</a>
	  			  		<a href="inscription">inscription</a>
	  		
	   <c:if test="${ not empty sessionScope.message }" >
		<p>${sessionScope.message}</p>
    </c:if>
    
	</c:if>
	
	
<c:forEach items="${ applicationScope.articles }" var="article">
	<div>
	<h4> ${ article.nom } </h4>
		<p> ${ article.description }, prix : ${ article.prix }</p>
	</div>
</c:forEach>	
</body>
</html>