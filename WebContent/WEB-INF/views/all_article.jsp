<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<a href="?action=create">Cree un article</a>
	<c:if test="${ not empty article }">
		<div>
			<h4>${ article.nom }</h4>
			<p>${ article.description }, prix : ${ article.prix }</p>
			<br>
							<a href="article">retour</a>
			
		</div>
	</c:if>
	<c:if test="${ empty article }">

		<c:forEach items="${ articles }" var="article">
			<div>
				<h4>${ article.nom }</h4>
				<p>${ article.description }, prix : ${ article.prix }</p>
				<a href="?action=update&id=${ article.id }">modifier</a>
				<a href="?action=delete&id=${ article.id }">supprimer</a>
				<a href="?id=${ article.id }">voir</a>
				
				<br>
			</div>
		</c:forEach>
	</c:if>
</body>
</html>